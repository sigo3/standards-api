﻿using System;

using Microsoft.AspNetCore.Mvc;

using Microsoft.Extensions.Logging;

using Domain.Standard.Exceptions;
using Domain.Standard.Models;
using Domain.Standard.Ports;


namespace WebApi.Controllers
{
    [Route("standard")]
    [ApiController]
    public class StandardController : ControllerBase
    {
        private readonly ILogger<StandardController> logger;
        private readonly ICreateStandardPort createStandardPort;
        private readonly IDeleteStandardPort deleteStandardPort;
        private readonly IFindStandardByCodePort findStandardByCodePort;
        private readonly IListStandardPort listStandardPort;
        private readonly IUpdateStandardPort updateStandardPort;

        public StandardController(
            ILogger<StandardController> logger,
            ICreateStandardPort createStandardPort,
            IDeleteStandardPort deleteStandardPort,
            IFindStandardByCodePort findStandardByCodePort,
            IListStandardPort listStandardPort,
            IUpdateStandardPort updateStandardPort
        )
        {
            this.logger = logger;
            this.createStandardPort = createStandardPort;
            this.deleteStandardPort = deleteStandardPort;
            this.findStandardByCodePort = findStandardByCodePort;
            this.listStandardPort = listStandardPort;
            this.updateStandardPort = updateStandardPort;
        }

        [HttpPost]
        public IActionResult CreateStandard([FromBody] Standard standard)
        {
            try
            {
                this.createStandardPort.CreateStandard(standard);
                return Created(nameof(CreateStandard), standard);
            }
            catch (StandardAlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("{code}")]
        public IActionResult UpdateStandard([FromRoute] string code, [FromBody] Standard standard)
        {
            try
            {
                this.updateStandardPort.UpdateStandard(code, standard);
                return Ok();
            }
            catch (StandardNotFoundException ex)
            {
                return NotFound(new
                {
                    Message = ex.Message
                });
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{code}")]
        public IActionResult FindStandardByCode([FromRoute] string code)
        {
            try
            {
                var standard = this.findStandardByCodePort.FindStandardByCode(code);
                return Ok(standard);
            }
            catch (StandardNotFoundException ex)
            {
                return NotFound(new
                {
                    Message = ex.Message
                });
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        public IActionResult ListStandard([FromQuery] int page, int pageSize)
        {
            try
            {
                var standards = this.listStandardPort.ListStandard(page, pageSize);
                return Ok(standards);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{code}")]
        public IActionResult DeleteStandard([FromRoute] string code)
        {
            try
            {
                this.deleteStandardPort.DeleteStandard(code);
                return NoContent();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
