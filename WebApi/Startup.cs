using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

using Arch.EntityFrameworkCore.UnitOfWork;

using AutoMapper;

using Domain.Standard.Adapters;
using Domain.Standard.Ports;

using Infrastructure.Common.Contexts;
using Infrastructure.Standard.Entities.Adapters;

using UseCase.Standard;


namespace WebApi
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureSettings(services);
            ConfigureSwagger(services);
            ConfigureDatabase(services);
            ConfigureAutoMapper(services);
            ConfigurePorts(services);
            ConfigureAdapters(services);

            services.AddLogging(configure =>
            {
                configure.AddConsole();
            });

            services.AddCors();
            services.AddControllers();
        }

        private void ConfigureSettings(IServiceCollection services)
        {
            //services.Configure<MySetting>(configuration.GetSection(nameof(MySetting)));
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "StandardApi",
                    Version = "v1"
                });
            });
        }

        private void ConfigureDatabase(IServiceCollection services)
        {
            string connectionString = configuration.GetConnectionString("MySqlContext");

            services.AddDbContext<MySqlContext>(options => options.UseMySql(connectionString));
            services.AddUnitOfWork<MySqlContext>();
        }

        private void ConfigureAutoMapper(IServiceCollection services)
        {
            IEnumerable<Type> profiles = Assembly.Load("Infrastructure").GetTypes()
                .Where(type => typeof(Profile).IsAssignableFrom(type));

            services.AddAutoMapper(profiles.ToArray());
        }

        private void ConfigurePorts(IServiceCollection services)
        {
            services.AddScoped<ICreateStandardPort, CreateStandardUseCase>();
            services.AddScoped<IDeleteStandardPort, DeleteStandardUseCase>();
            services.AddScoped<IFindStandardByCodePort, FindStandardUseCase>();
            services.AddScoped<IListStandardPort, ListStandardUseCase>();
            services.AddScoped<IUpdateStandardPort, UpdateStandardUseCase>();
        }

        private void ConfigureAdapters(IServiceCollection services)
        {
            services.AddScoped<ICreateStandardAdapter, MySqlAdapter>();
            services.AddScoped<IDeleteStandardAdapter, MySqlAdapter>();
            services.AddScoped<IFindStandardAdapter, MySqlAdapter>();
            services.AddScoped<IListStandardAdapter, MySqlAdapter>();
            services.AddScoped<IUpdateStandardAdapter, MySqlAdapter>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseCors(option => {
                option.AllowAnyOrigin();
                option.AllowAnyMethod();
                option.AllowAnyHeader();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "StandardsApi");
            });
        }
    }
}
