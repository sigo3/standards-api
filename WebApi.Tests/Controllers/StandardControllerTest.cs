using System;
using System.Net;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Domain.Standard.Exceptions;
using Domain.Standard.Models;
using Domain.Standard.Ports;

using FizzWare.NBuilder;

using FluentAssertions;

using Moq;

using WebApi.Controllers;

using Xunit;
using Domain.Common;

namespace WebApi.Tests.Controllers
{
    public class StandardControllerTest
    {

        private readonly Mock<ILogger<StandardController>> loggerMock;
        private readonly Mock<ICreateStandardPort> createStandardPortMock;
        private readonly Mock<IDeleteStandardPort> deleteStandardPortMock;
        private readonly Mock<IFindStandardByCodePort> findStandardByCodePortMock;
        private readonly Mock<IListStandardPort> listStandardPortMock;
        private readonly Mock<IUpdateStandardPort> updateStandardPortMock;

        private readonly StandardController standardController;

        public StandardControllerTest()
        {
            this.loggerMock = new Mock<ILogger<StandardController>>();
            this.createStandardPortMock = new Mock<ICreateStandardPort>();
            this.deleteStandardPortMock = new Mock<IDeleteStandardPort>();
            this.findStandardByCodePortMock = new Mock<IFindStandardByCodePort>();
            this.listStandardPortMock = new Mock<IListStandardPort>();
            this.updateStandardPortMock = new Mock<IUpdateStandardPort>();

            this.standardController = new StandardController(
                this.loggerMock.Object,
                this.createStandardPortMock.Object,
                this.deleteStandardPortMock.Object,
                this.findStandardByCodePortMock.Object,
                this.listStandardPortMock.Object,
                this.updateStandardPortMock.Object
            );
        }

        [Fact]
        public void TestCreateStandardReturnCreated()
        {
            var standard = Builder<Standard>.CreateNew().Build();

            var result = standardController.CreateStandard(standard) as CreatedResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.Created);
        }

        [Fact]
        public void TestCreateStandardReturnBadRequest()
        {
            var standard = Builder<Standard>.CreateNew().Build();

            createStandardPortMock.Setup(
                mock => mock.CreateStandard(It.IsAny<Standard>())
            ).Throws(new StandardAlreadyExistsException(standard));

            var result = standardController.CreateStandard(standard) as BadRequestObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void TestCreateStandardReturnInternalServerError()
        {
            var standard = Builder<Standard>.CreateNew().Build();

            createStandardPortMock.Setup(
                mock => mock.CreateStandard(It.IsAny<Standard>())
            ).Throws<Exception>();

            var result = standardController.CreateStandard(standard) as ObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
        }

        [Fact]
        public void TestUpdateStandardReturnOk()
        {
            var standard = Builder<Standard>.CreateNew().Build();

            var result = standardController.UpdateStandard(standard.Code, standard) as OkResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Fact]
        public void TestUpdateStandardReturnNotFound()
        {
            var standard = Builder<Standard>.CreateNew().Build();

            updateStandardPortMock.Setup(
                mock => mock.UpdateStandard(It.IsAny<string>(), It.IsAny<Standard>())
            ).Throws(new StandardNotFoundException(standard.Code));

            var result = standardController.UpdateStandard(standard.Code, standard) as NotFoundObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }

        [Fact]
        public void TestUpdateStandardReturnInternalServerError()
        {
            var standard = Builder<Standard>.CreateNew().Build();

            updateStandardPortMock.Setup(
                mock => mock.UpdateStandard(It.IsAny<string>(), It.IsAny<Standard>())
            ).Throws<Exception>();

            var result = standardController.UpdateStandard(standard.Code, standard) as ObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
        }

        [Fact]
        public void TestFindStandardByCodeReturnOk()
        {
            var standard = Builder<Standard>.CreateNew().Build();

            findStandardByCodePortMock.Setup(
                mock => mock.FindStandardByCode(It.IsAny<string>())
            ).Returns(standard);

            var result = standardController.FindStandardByCode(standard.Code) as OkObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Fact]
        public void TestFindStandardByCodeReturnNotFound()
        {
            var standard = Builder<Standard>.CreateNew().Build();

            findStandardByCodePortMock.Setup(
                mock => mock.FindStandardByCode(It.IsAny<string>())
            ).Throws(new StandardNotFoundException(standard.Code));

            var result = standardController.FindStandardByCode(standard.Code) as NotFoundObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }

        [Fact]
        public void TestFindStandardByCodeReturnInternalServerError()
        {
            var standard = Builder<Standard>.CreateNew().Build();

            findStandardByCodePortMock.Setup(
                mock => mock.FindStandardByCode(It.IsAny<string>())
            ).Throws<Exception>();

            var result = standardController.FindStandardByCode(standard.Code) as ObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
        }

        [Fact]
        public void TestListStandardReturnOk()
        {
            var standards = Builder<PaginatedResult<Standard>>.CreateNew().Build();

            listStandardPortMock.Setup(
                mock => mock.ListStandard(It.IsAny<int>(), It.IsAny<int>())
            ).Returns(standards);

            var result = standardController.ListStandard(0, 10) as OkObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Fact]
        public void TestListStandardReturnInternalServerError()
        {
            listStandardPortMock.Setup(
                mock => mock.ListStandard(It.IsAny<int>(), It.IsAny<int>())
            ).Throws<Exception>();

            var result = standardController.ListStandard(0, 10) as ObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
        }
    }
}
