using System.Net;

using Microsoft.AspNetCore.Mvc;

using FluentAssertions;

using WebApi.Controllers;

using Xunit;

namespace WebApi.Tests.Controllers
{
    public class PingControllerTest
    {
        [Fact]
        public void TestPingReturnOk()
        {
            var result = new PingController().Ping() as OkResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }
    }
}
