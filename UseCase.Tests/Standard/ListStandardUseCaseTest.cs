using Domain.Standard.Adapters;

using FizzWare.NBuilder;

using Moq;

using UseCase.Standard;

using Xunit;

namespace UseCase.Tests.Standard
{
    public class ListStandardUseCaseTest
    {
        private readonly Mock<IListStandardAdapter> listStandardAdapterMock;

        private readonly ListStandardUseCase listStandardUseCase;

        public ListStandardUseCaseTest()
        {
            this.listStandardAdapterMock = new Mock<IListStandardAdapter>();

            this.listStandardUseCase = new ListStandardUseCase(
                this.listStandardAdapterMock.Object
            );
        }

        [Fact]
        public void TestListStandardWithSuccess()
        {
            var standard = Builder<Domain.Standard.Models.Standard>.CreateNew().Build();

            listStandardUseCase.ListStandard(It.IsAny<int>(), It.IsAny<int>());

            listStandardAdapterMock.Verify(
                mock => mock.ListStandard(It.IsAny<int>(), It.IsAny<int>()), Times.Once()
            );
        }
    }
}
