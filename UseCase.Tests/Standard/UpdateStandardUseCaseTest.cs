using Domain.Standard.Adapters;
using Domain.Standard.Exceptions;

using FizzWare.NBuilder;

using Moq;

using UseCase.Standard;

using Xunit;

namespace UseCase.Tests.Standard
{
    public class UpdateStandardUseCaseTest
    {
        private readonly Mock<IUpdateStandardAdapter> updateStandardAdapterMock;

        private readonly UpdateStandardUseCase updateStandardUseCase;

        public UpdateStandardUseCaseTest()
        {
            this.updateStandardAdapterMock = new Mock<IUpdateStandardAdapter>();

            this.updateStandardUseCase = new UpdateStandardUseCase(
                this.updateStandardAdapterMock.Object
            );
        }

        [Fact]
        public void TestUpdateStandardWithSuccess()
        {
            var standard = Builder<Domain.Standard.Models.Standard>.CreateNew().Build();

            updateStandardUseCase.UpdateStandard(standard.Code, standard);

            updateStandardAdapterMock.Verify(
                mock => mock.UpdateStandard(It.IsAny<string>(), It.IsAny<Domain.Standard.Models.Standard>()), Times.Once()
            );
        }

        [Fact]
        public void TestUpdateStandardNotFound()
        {
            var standard = Builder<Domain.Standard.Models.Standard>.CreateNew().Build();

            updateStandardAdapterMock.Setup(
                mock => mock.UpdateStandard(standard.Code, It.IsAny<Domain.Standard.Models.Standard>())
            ).Throws(new StandardNotFoundException(standard.Code));

            Assert.Throws<StandardNotFoundException>(() =>
            {
                updateStandardUseCase.UpdateStandard(standard.Code, standard);
            });
        }
    }
}
