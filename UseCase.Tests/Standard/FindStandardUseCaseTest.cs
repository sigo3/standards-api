using Domain.Standard.Adapters;
using Domain.Standard.Exceptions;

using FizzWare.NBuilder;

using Moq;

using UseCase.Standard;

using Xunit;

namespace UseCase.Tests.Standard
{
    public class FindStandardUseCaseTest
    {
        private readonly Mock<IFindStandardAdapter> findStandardAdapterMock;

        private readonly FindStandardUseCase findStandardUseCase;

        public FindStandardUseCaseTest()
        {
            this.findStandardAdapterMock = new Mock<IFindStandardAdapter>();

            this.findStandardUseCase = new FindStandardUseCase(
                this.findStandardAdapterMock.Object
            );
        }

        [Fact]
        public void TestFindStandardWithSuccess()
        {
            var standard = Builder<Domain.Standard.Models.Standard>.CreateNew().Build();

            findStandardAdapterMock.Setup(
                mock => mock.FindStandardByCode(It.IsAny<string>())
            ).Returns(standard);

            findStandardUseCase.FindStandardByCode(standard.Code);

            findStandardAdapterMock.Verify(
                mock => mock.FindStandardByCode(It.IsAny<string>()), Times.Once()
            );
        }

        [Fact]
        public void TestFindStandardNotFound()
        {
            var standard = Builder<Domain.Standard.Models.Standard>.CreateNew().Build();

            Assert.Throws<StandardNotFoundException>(() =>
            {
                findStandardUseCase.FindStandardByCode(standard.Code);
            });
        }
    }
}
