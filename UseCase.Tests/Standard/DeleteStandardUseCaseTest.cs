using Domain.Standard.Adapters;

using FizzWare.NBuilder;

using Moq;

using UseCase.Standard;

using Xunit;

namespace UseCase.Tests.Standard
{
    public class DeleteStandardUseCaseTest
    {
        private readonly Mock<IDeleteStandardAdapter> deleteStandardAdapterMock;

        private readonly DeleteStandardUseCase deleteStandardUseCase;

        public DeleteStandardUseCaseTest()
        {
            this.deleteStandardAdapterMock = new Mock<IDeleteStandardAdapter>();

            this.deleteStandardUseCase = new DeleteStandardUseCase(
                this.deleteStandardAdapterMock.Object
            );
        }

        [Fact]
        public void TestDeleteStandardWithSuccess()
        {
            var standard = Builder<Domain.Standard.Models.Standard>.CreateNew().Build();

            deleteStandardUseCase.DeleteStandard(standard.Code);

            deleteStandardAdapterMock.Verify(
                mock => mock.DeleteStandard(It.IsAny<string>()), Times.Once()
            );
        }
    }
}
