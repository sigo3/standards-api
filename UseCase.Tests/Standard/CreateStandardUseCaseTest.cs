using Domain.Standard.Adapters;
using Domain.Standard.Exceptions;

using FizzWare.NBuilder;

using Moq;

using UseCase.Standard;

using Xunit;

namespace UseCase.Tests.Standard
{
    public class CreateStandardUseCaseTest
    {
        private readonly Mock<IFindStandardAdapter> findStandardAdapterMock;
        private readonly Mock<ICreateStandardAdapter> createStandardAdapterMock;

        private readonly CreateStandardUseCase createStandardUseCase;

        public CreateStandardUseCaseTest()
        {
            this.findStandardAdapterMock = new Mock<IFindStandardAdapter>();
            this.createStandardAdapterMock = new Mock<ICreateStandardAdapter>();

            this.createStandardUseCase = new CreateStandardUseCase(
                this.findStandardAdapterMock.Object,
                this.createStandardAdapterMock.Object
            );
        }

        [Fact]
        public void TestCreateStandardWithSuccess()
        {
            var standard = Builder<Domain.Standard.Models.Standard>.CreateNew().Build();

            createStandardUseCase.CreateStandard(standard);

            createStandardAdapterMock.Verify(
                mock => mock.CreateStandard(It.IsAny<Domain.Standard.Models.Standard>()), Times.Once()
            );
        }

        [Fact]
        public void TestCreateStandardAlreadyExists()
        {
            var standard = Builder<Domain.Standard.Models.Standard>.CreateNew().Build();

            findStandardAdapterMock.Setup(
                mock => mock.FindStandardByCode(It.IsAny<string>())
            ).Returns(standard);

            Assert.Throws<StandardAlreadyExistsException>(() =>
            {
                createStandardUseCase.CreateStandard(standard);
            });
        }
    }
}
