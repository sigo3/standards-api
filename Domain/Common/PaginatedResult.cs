using System.Collections.Generic;

namespace Domain.Common
{
    public class PaginatedResult<TModel>
    {
        public IEnumerable<TModel> Items { get; set; }

        public int TotalCount { get; set; }

        public int TotalPages { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
