namespace Domain.Standard.Ports
{
    public interface ICreateStandardPort
    {
        void CreateStandard(Standard.Models.Standard standard);
    }
}
