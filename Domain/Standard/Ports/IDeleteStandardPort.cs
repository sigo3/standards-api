namespace Domain.Standard.Ports
{
    public interface IDeleteStandardPort
    {
        void DeleteStandard(string code);
    }
}
