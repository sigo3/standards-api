namespace Domain.Standard.Ports
{
    public interface IFindStandardByCodePort
    {
        Standard.Models.Standard FindStandardByCode(string code);
    }
}
