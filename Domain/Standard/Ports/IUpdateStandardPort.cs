namespace Domain.Standard.Ports
{
    public interface IUpdateStandardPort
    {
        void UpdateStandard(string code, Standard.Models.Standard standard);
    }
}
