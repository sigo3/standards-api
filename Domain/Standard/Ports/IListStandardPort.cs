using Domain.Common;

namespace Domain.Standard.Ports
{
    public interface IListStandardPort
    {
        PaginatedResult<Standard.Models.Standard> ListStandard(int page, int pageSize);
    }
}
