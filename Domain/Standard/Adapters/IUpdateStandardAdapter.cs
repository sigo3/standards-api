namespace Domain.Standard.Adapters
{
    public interface IUpdateStandardAdapter
    {
        void UpdateStandard(string code, Standard.Models.Standard standard);
    }
}
