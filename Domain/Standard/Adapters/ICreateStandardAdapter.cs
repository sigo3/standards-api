namespace Domain.Standard.Adapters
{
    public interface ICreateStandardAdapter
    {
        void CreateStandard(Standard.Models.Standard standard);
    }
}
