namespace Domain.Standard.Adapters
{
    public interface IDeleteStandardAdapter
    {
        void DeleteStandard(string code);
    }
}
