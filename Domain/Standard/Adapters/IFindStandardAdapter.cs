namespace Domain.Standard.Adapters
{
    public interface IFindStandardAdapter
    {
        Standard.Models.Standard FindStandardByCode(string code);
    }
}
