using Domain.Common;

namespace Domain.Standard.Adapters
{
    public interface IListStandardAdapter
    {
        PaginatedResult<Standard.Models.Standard> ListStandard(int page, int pageSize);
    }
}
