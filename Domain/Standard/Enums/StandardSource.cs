namespace Domain.Standard.Enums
{
    public enum StandardSource
    {
        Internal,
        External
    }
}
