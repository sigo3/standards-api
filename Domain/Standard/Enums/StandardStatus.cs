namespace Domain.Standard.Enums
{
    public enum StandardStatus
    {
        PendingApproval,
        Outdated,
        Operative,
        NotOperative
    }
}
