using System;

namespace Domain.Standard.Exceptions
{
    public class StandardAlreadyExistsException : Exception
    {
        public StandardAlreadyExistsException(Standard.Models.Standard standard) : base($"Standard with code {standard.Code} already exists.") { }
    }
}
