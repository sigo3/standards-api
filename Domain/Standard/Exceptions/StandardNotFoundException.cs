using System;

namespace Domain.Standard.Exceptions
{
    public class StandardNotFoundException : Exception
    {
        public StandardNotFoundException(string code) : base($"Standard with code {code} not found") { }
    }
}
