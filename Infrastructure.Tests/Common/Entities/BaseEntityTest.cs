using FizzWare.NBuilder;

using FluentAssertions;

using Infrastructure.Common.Entities;

using Xunit;

namespace Infrastructure.Tests.Common.Entities
{
    public class BaseEntityTest
    {
        private readonly BaseEntity baseEntity;

        public BaseEntityTest()
        {
            this.baseEntity = Builder<Infrastructure.Standard.Entities.Standard>.CreateNew().Build();
        }

        [Fact]
        public void TestCopyValues()
        {
            var entity = Builder<Infrastructure.Standard.Entities.Standard>.CreateNew().Build();
            var model = new Domain.Standard.Models.Standard();

            BaseEntity.Copy<Infrastructure.Standard.Entities.Standard, Domain.Standard.Models.Standard>(entity, model);

            model.Code.Should().Be(entity.Code);
            model.Title.Should().Be(entity.Title);
            model.Description.Should().Be(entity.Description);
            model.Committee.Should().Be(entity.Committee);
            model.Url.Should().Be(entity.Url);
            model.PublishDate.Should().Be(entity.PublishDate);
            model.Status.Should().Be(entity.Status);
            model.Source.Should().Be(entity.Source);
        }
    }
}
