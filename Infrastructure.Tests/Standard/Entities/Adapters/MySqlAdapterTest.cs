using Arch.EntityFrameworkCore.UnitOfWork;

using AutoMapper;

using FizzWare.NBuilder;

using Infrastructure.Common.Contexts;
using Infrastructure.Standard.Entities.Adapters;

using Moq;

using Xunit;

namespace Infrastructure.Tests.Standard.Entities.Adapters
{
    public class MySqlAdapterTest
    {
        private readonly Mock<IUnitOfWork<MySqlContext>> unitOfWorkMock;
        private readonly Mock<IRepository<Infrastructure.Standard.Entities.Standard>> standardRepositoryMock;
        private readonly Mock<IMapper> mapperMock;

        private readonly MySqlAdapter mySqlAdapter;

        public MySqlAdapterTest()
        {
            this.unitOfWorkMock = new Mock<IUnitOfWork<MySqlContext>>();
            this.standardRepositoryMock = new Mock<IRepository<Infrastructure.Standard.Entities.Standard>>();
            this.mapperMock = new Mock<IMapper>();

            this.unitOfWorkMock.Setup(
                mock => mock.GetRepository<Infrastructure.Standard.Entities.Standard>(false)
            ).Returns(this.standardRepositoryMock.Object);

            this.mySqlAdapter = new MySqlAdapter(
                this.unitOfWorkMock.Object,
                this.mapperMock.Object
            );
        }

        [Fact]
        public void TestCreateStandardWithSuccess()
        {
            var entity = Builder<Infrastructure.Standard.Entities.Standard>.CreateNew().Build();
            var model = new Domain.Standard.Models.Standard();

            Infrastructure.Standard.Entities.Standard.Copy<Infrastructure.Standard.Entities.Standard, Domain.Standard.Models.Standard>(entity, model);

            mySqlAdapter.CreateStandard(model);

            standardRepositoryMock.Verify(
                mock => mock.Insert(It.IsAny<Infrastructure.Standard.Entities.Standard>()), Times.Once()
            );

            unitOfWorkMock.Verify(
                mock => mock.SaveChanges(false), Times.Once()
            );
        }

        [Fact]
        public void TestDeleteStandardWithSuccess()
        {
            var standard = Builder<Infrastructure.Standard.Entities.Standard>.CreateNew().Build();

            mySqlAdapter.DeleteStandard(standard.Code);

            standardRepositoryMock.Verify(
                mock => mock.Delete(It.IsAny<Infrastructure.Standard.Entities.Standard>()), Times.Once()
            );

            unitOfWorkMock.Verify(
                mock => mock.SaveChanges(false), Times.Once()
            );
        }

        [Fact(Skip="Out of scope")]
        public void TestFindStandardByCodeStandardWithSuccess()
        {
            var standard = Builder<Infrastructure.Standard.Entities.Standard>.CreateNew().Build();

            mySqlAdapter.FindStandardByCode(standard.Code);

            standardRepositoryMock.Verify(
                mock => mock.GetFirstOrDefault(
                    _standard => _standard.Code == standard.Code,
                    null,
                    null,
                    true,
                    false
                ), Times.Once()
            );
        }

        [Fact(Skip="Out of scope")]
        public void TestListStandardWithSuccess()
        {
            mySqlAdapter.ListStandard(0, 20);

            standardRepositoryMock.Verify(
                mock => mock.GetPagedList<Infrastructure.Standard.Entities.Standard>(
                    _standard => _standard,
                    null,
                    null,
                    null,
                    0,
                    20,
                    true,
                    false
                ), Times.Once()
            );
        }

        [Fact(Skip="Out of scope")]
        public void TestUpdateStandardWithSuccess()
        {
            var entity = Builder<Infrastructure.Standard.Entities.Standard>.CreateNew().Build();
            var model = new Domain.Standard.Models.Standard();

            Infrastructure.Standard.Entities.Standard.Copy<Infrastructure.Standard.Entities.Standard, Domain.Standard.Models.Standard>(entity, model);

            standardRepositoryMock.Setup(
                mock => mock.GetFirstOrDefault(
                    _standard => _standard.Code == entity.Code,
                    null,
                    null,
                    true,
                    false
                )
            ).Returns(entity);

            mySqlAdapter.UpdateStandard(model.Code, model);

            standardRepositoryMock.Verify(
                mock => mock.Update(It.IsAny<Infrastructure.Standard.Entities.Standard>()), Times.Once()
            );

            unitOfWorkMock.Verify(
                mock => mock.SaveChanges(false), Times.Once()
            );
        }
    }
}
