using AutoMapper;

namespace Infrastructure.Common.Mappers
{
    public class StandardProfile : Profile
    {
        public StandardProfile()
        {
            CreateMap<Domain.Standard.Models.Standard, Infrastructure.Standard.Entities.Standard>().ReverseMap();
        }
    }
}
