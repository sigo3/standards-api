using System;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Common.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public static void Copy<TSource, TTarget>(TSource source, TTarget target)
        {
            var sourceProperties = source.GetType().GetProperties();
            var targetProperties = target.GetType().GetProperties();

            foreach (var sourceProperty in sourceProperties)
            {
                foreach (var targetProperty in targetProperties)
                {
                    if (sourceProperty.Name == targetProperty.Name && sourceProperty.PropertyType == targetProperty.PropertyType)
                    {
                        var val = sourceProperty.GetValue(source);
                        if (val == null)
                        {
                            continue;
                        }
                        targetProperty.SetValue(target, val);
                    }
                }
            }
        }
    }
}
