using System.Collections.Generic;

using Arch.EntityFrameworkCore.UnitOfWork;

using AutoMapper;

using Domain.Standard.Adapters;
using Models = Domain.Standard.Models;

using Infrastructure.Common.Contexts;
using Domain.Common;
using Domain.Standard.Exceptions;

namespace Infrastructure.Standard.Entities.Adapters
{
    public class MySqlAdapter :
        ICreateStandardAdapter,
        IDeleteStandardAdapter,
        IFindStandardAdapter,
        IListStandardAdapter,
        IUpdateStandardAdapter
    {
        private readonly IUnitOfWork<MySqlContext> unitOfWork;
        private readonly IMapper mapper;

        public MySqlAdapter(IUnitOfWork<MySqlContext> unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public void CreateStandard(Models.Standard standard)
        {
            unitOfWork.GetRepository<Entities.Standard>().Insert(
                mapper.Map<Models.Standard, Entities.Standard>(standard)
            );

            unitOfWork.SaveChanges();
        }

        public void DeleteStandard(string code)
        {
            var standard = unitOfWork.GetRepository<Entities.Standard>().GetFirstOrDefault(
                predicate: _standard => _standard.Code == code
            );

            unitOfWork.GetRepository<Standard>().Delete(standard);

            unitOfWork.SaveChanges();
        }

        public Models.Standard FindStandardByCode(string code)
        {
            var standard = unitOfWork.GetRepository<Entities.Standard>().GetFirstOrDefault(
                predicate: _standard => _standard.Code == code
            );

            return mapper.Map<Entities.Standard, Models.Standard>(standard);
        }

        public PaginatedResult<Models.Standard> ListStandard(int page, int pageSize)
        {
            var result = unitOfWork.GetRepository<Entities.Standard>()
                .GetPagedList<Entities.Standard>(
                    selector: _standard => _standard,
                    pageIndex: page,
                    pageSize: pageSize
                );

            return new PaginatedResult<Models.Standard>
            {
                Items = mapper.Map<IEnumerable<Entities.Standard>, IEnumerable<Models.Standard>>(result.Items),
                TotalCount = result.TotalCount,
                TotalPages = result.TotalPages,
                Page = result.PageIndex,
                PageSize = result.PageSize
            };
        }

        public void UpdateStandard(string code, Models.Standard standard)
        {
            var foundStandard = unitOfWork.GetRepository<Entities.Standard>().GetFirstOrDefault(
                predicate: _standard => _standard.Code == code
            );

            if (foundStandard == null)
            {
                throw new StandardNotFoundException(standard.Code);
            }

            Entities.Standard.Copy<Models.Standard, Entities.Standard>(standard, foundStandard);

            unitOfWork.GetRepository<Entities.Standard>().Update(foundStandard);

            unitOfWork.SaveChanges();
        }
    }
}
