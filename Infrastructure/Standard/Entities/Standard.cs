using System;

using Domain.Standard.Enums;

using Infrastructure.Common.Entities;

namespace Infrastructure.Standard.Entities
{
    public class Standard : BaseEntity
    {
        public string Code { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Committee { get; set; }

        public string Url { get; set; }

        public DateTime PublishDate { get; set; }

        public StandardStatus Status { get; set; }

        public StandardSource Source { get; set; }
    }
}
