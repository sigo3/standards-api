using Domain.Common;
using Domain.Standard.Adapters;
using Domain.Standard.Ports;

namespace UseCase.Standard
{
    public class ListStandardUseCase : IListStandardPort
    {
        private readonly IListStandardAdapter listStandardAdapter;

        public ListStandardUseCase(IListStandardAdapter listStandardAdapter)
        {
            this.listStandardAdapter = listStandardAdapter;
        }

        public PaginatedResult<Domain.Standard.Models.Standard> ListStandard(int page, int pageSize)
        {
            return this.listStandardAdapter.ListStandard(page, pageSize);
        }
    }
}
