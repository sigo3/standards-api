using Domain.Standard.Adapters;
using Domain.Standard.Ports;

namespace UseCase.Standard
{
    public class UpdateStandardUseCase : IUpdateStandardPort
    {
        private readonly IUpdateStandardAdapter updateStandardAdapter;

        public UpdateStandardUseCase(IUpdateStandardAdapter updateStandardAdapter)
        {
            this.updateStandardAdapter = updateStandardAdapter;
        }

        public void UpdateStandard(string code, Domain.Standard.Models.Standard standard)
        {
            this.updateStandardAdapter.UpdateStandard(code, standard);
        }
    }
}
