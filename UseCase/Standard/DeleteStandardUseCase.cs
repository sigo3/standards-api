using Domain.Standard.Adapters;
using Domain.Standard.Ports;

namespace UseCase.Standard
{
    public class DeleteStandardUseCase : IDeleteStandardPort
    {
        private readonly IDeleteStandardAdapter deleteStandardAdapter;

        public DeleteStandardUseCase(IDeleteStandardAdapter deleteStandardAdapter)
        {
            this.deleteStandardAdapter = deleteStandardAdapter;
        }

        public void DeleteStandard(string code)
        {
            this.deleteStandardAdapter.DeleteStandard(code);
        }
    }
}
