using Domain.Standard.Adapters;
using Domain.Standard.Exceptions;
using Domain.Standard.Ports;

namespace UseCase.Standard
{
    public class CreateStandardUseCase : ICreateStandardPort
    {
        private readonly IFindStandardAdapter findStandardAdapter;
        private readonly ICreateStandardAdapter createStandardAdapter;

        public CreateStandardUseCase(IFindStandardAdapter findStandardAdapter, ICreateStandardAdapter createStandardAdapter)
        {
            this.findStandardAdapter = findStandardAdapter;
            this.createStandardAdapter = createStandardAdapter;
        }

        public void CreateStandard(Domain.Standard.Models.Standard standard)
        {
            var foundStandard = this.findStandardAdapter.FindStandardByCode(standard.Code);
            if (foundStandard != null)
            {
                throw new StandardAlreadyExistsException(foundStandard);
            }

            this.createStandardAdapter.CreateStandard(standard);
        }
    }
}
